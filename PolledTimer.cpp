//**************************************************************
//  PolledTimer.h  
//  Provides basic timing related methods
//
//  David Fowler 09/07/2010
//  Version 0.2
//  Converted into an Arudino library and renamed to PolledTimer
//
//  David Fowler 11/18/09
//  Version 0.1
//**************************************************************
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include "PolledTimer.h"

PolledTimer::PolledTimer(unsigned long timeoutMS){
    Reset();
	SetTimeout(timeoutMS);
	callback=NULL;
}

void PolledTimer::SetTimeout(unsigned long timeoutMS){
  intervalMS=timeoutMS;
}

void PolledTimer::Reset(void){
  timeMark=millis();
}

int PolledTimer::HasElapsed(){
  unsigned long timeCurrent;
  unsigned long timeElapsed;
  int result=false;
  
  timeCurrent=millis();
//  if(timeCurrent<timeMark) {  //Rollover detected
//    timeElapsed=(TIMECTL_MAXTICKS-timeMark)+timeCurrent;  //elappsed=all the ticks to overflow + all the ticks since overflow
//  }
//  else {
    timeElapsed=timeCurrent-timeMark;  
//  }

  if(timeElapsed>=intervalMS) {
    Reset();
    result=true;
  }
  return(result);  
}

void PolledTimer::Update(void){
  if (callback!=NULL) {
    if(HasElapsed()) {
	  callback();
	}
  }
}

void PolledTimer::SetCallback(void (*callbackFunction)(void)){
  callback=callbackFunction;
}
