//**************************************************************
//  PolledTimer.h  
//  Provides basic timing related methods
//
//  David Fowler 09/07/2010
//  Version 0.2
//  Converted into an Arudino library and renamed to PolledTimer
//
//  David Fowler 11/18/09
//  Version 0.1
//**************************************************************
#ifndef PolledTimer_h
#define PolledTimer_h

//#define TIMECTL_MAXTICKS  ((unsigned)4294967295L)
#define TIMECTL_INIT      0

class PolledTimer {
  private:
    unsigned long timeMark;
	unsigned long intervalMS;
	void (*callback)(void);
  public:
    PolledTimer(unsigned long timeoutMS);
	void SetTimeout(unsigned long timeoutMS);
	void Reset(void);
	int HasElapsed();
	void Update();
	void SetCallback(void (*callbackFunction)(void));
};

#endif