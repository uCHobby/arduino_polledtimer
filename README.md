# Arduino PolledTimer Library #
Version 1.0 by David Fowler aka [uCHobby](http://www.uchobby.com).

Visit the [uCHobby](http://www.uchobby.com) web site for more related goodies

A simple library to control timing in your project.  Use a PolledTimer object rather than delays in your code.

This is a version of a common code module I use often in projects where I don't have an OS or other system service to handle threading and timing.

It's very simple and subtle but can totally change the way you code. To a better more event driven style.

### How do I get set up? ###
Install as an external ZIP library in the Arduino IDE.  Download the ZIP by selecting downloads on the left navigation menu, then click on "PolledTimer.zip"  Install the Zip as a library in the Arduino IDE by selecting menu item Sketch/Include Library/Add Zip Library. After the library is installed you should be able to add a reference to it from the Sketch menu.  You may have to scroll down a bit.

### License ###
You are free to use this code as is, without warrantee of any sort. We only request that you mention the uCHobby web site. At the minimum it would be nice to hear from you.

### Usage ###
	#include <PolledTimer.h>  //This references the Polled Timer Lib
                 		      //You will get this header automatically with you 
                        	  //add the library to your sketch from the menu

	const int pinLED = 13;    //constant for our LED to blink.  I/O 13 is usually an LED on Arduino's
    	                      //We use const here instead of #define, but either way works

	PolledTimer timerBlink(500);  //Create a timer specifying the name and the interval in milliseconds

	void OnTimerBlink(void);  //A prototype for the callback method

	void setup() {            //Standard Arduino setup function, called once at startup.
  		pinMode(pinLED,OUTPUT); //Set the I/O pin to output mode to drive the LED
  
  		timerBlink.SetCallback(OnTimerBlink);  //Set the timer callback function, must me void function(void)
	}

	void loop() { //Standard Arduino loop function, called constantly.
	  timerBlink.Update();  //Call the timers Update method. This is where the magic happens
    	                    //the Update method will call the registered callback at the specified interval 
                        
		//Here you can do anything you like, the LED will flash without stalling your code. No delay required.                        
	}

	void OnTimerBlink(void){  //Here we have the Blinking callback.
  		bool ledState;          //The LED's state
  
  		ledState=digitalRead(pinLED); //Read the current LED state
  		ledState=!ledState;           //Use Boolean Not to invert it.
  		digitalWrite(pinLED,ledState);//Set the LED to the new state.
	}


### Contribution guidelines ###
Send an email with your ideas.

### Who do I talk to? ###
You can send me an email at david.fowler@gmail.com. Put Arduino_PolledTimer in the subject line to keep it from being deleted as spam.

I would love to hear what you think and welcome suggestions.

Tyring to make this a 1.5.X compatable lib.. Look here for info.
https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5:-Library-specification