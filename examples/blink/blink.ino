#include <PolledTimer.h>  //This references the Polled Timer Lib
                          //You will get this header automatically with you 
                          //add the library to your sketch from the menu

const int pinLED = 13;    //constant for our LED to blink.  I/O 13 is usually an LED on Arduino's
                          //We use const here instead of #define, but either way works

PolledTimer timerBlink(500);  //Create a timer specifying the name and the interval in milliseconds

void OnTimerBlink(void);  //A prototype for the callback method

void setup() {            //Standard Arduino setup function, called once at startup.
  pinMode(pinLED,OUTPUT); //Set the I/O pin to output mode to drive the LED
  
  timerBlink.SetCallback(OnTimerBlink);  //Set the timer callback function, must me void function(void)
}

void loop() { //Standard Arduino loop function, called constantly.
  timerBlink.Update();  //Call the timers Update method. This is where the magic happens
                        //the Update method will call the registered callback at the specified interval 
                        
//Here you can do anything you like, the LED will flash without stalling your code. No delay required.                        
}

void OnTimerBlink(void){  //Here we have the Blinking callback.
  bool ledState;          //The LED's state
  
  ledState=digitalRead(pinLED); //Read the current LED state
  ledState=!ledState;           //Use Boolean Not to invert it.
  digitalWrite(pinLED,ledState);//Set the LED to the new state.

}
